import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestContactService {
    private ContactService contactService = new ContactService();

    @Test
    public void shouldFaillIfNull(){
        Assertions.assertThrows(ContactException.class, () -> contactService.creerContact(null));
    }

    @Test
    public void shouldFaillIfEmpty(){
        Assertions.assertThrows(ContactException.class, () -> contactService.creerContact(""));
    }

    @Test
    public void shouldFaillIfNameIsUnderThree(){
        Assertions.assertThrows(ContactException.class, () -> contactService.creerContact("11"));
    }
    @Test
    public void shouldFaillIfContaintSpecialChar(){
        Assertions.assertThrows(ContactException.class, () -> contactService.creerContact("$$25"));
    }

    @Test
    public void shouldFaillIfContaintMore40Char(){
        Assertions.assertThrows(ContactException.class, () -> contactService.creerContact("12345678913456789123456789123456789123456789123456789"));
    }

    @Test
    public void shouldOkIfGoodLength() throws ContactException {
        contactService.creerContact("abcd");
    }

    @Test
    public void shouldOkIf40Char() throws ContactException {
        contactService.creerContact("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
    }

    @Test
    public void shouldFaillIfEntryNameTwice() throws ContactException{
        contactService.creerContact("thierry");
        Assertions.assertThrows(ContactException.class, () -> contactService.creerContact("thierry"));
    }

}
