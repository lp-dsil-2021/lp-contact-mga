import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.aggregator.ArgumentAccessException;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ContactServiceMockTest {
    @Mock
    public IContactDAO contactDAO;

    @InjectMocks
    public ContactService contactService = new ContactService();

    @Captor
    private ArgumentCaptor<Contact> contactCaptor;

    @Test(expected = ContactException.class)
    public void shouldFailOnDuplicateEntry() throws ContactException {
        //Def
        Mockito.when(contactDAO.isContactExists("thierry")).thenReturn(true);
        //Test
        contactService.creerContact("thierry");
    }
    @Test
    public void shouldOKOnlyOneEntry() throws ContactException{
        Mockito.when(contactDAO.isContactExists("thierry")).thenReturn(false);
        contactService.creerContact("thierry");

        Mockito.verify(contactDAO).add(contactCaptor.capture());
        Contact c = contactCaptor.getValue();
        Assertions.assertEquals("thierry", c.getNom());
    }

    @Test(expected = ContactException.class)
    public void shouldFailIfWantContactNotExit() throws ContactException{
        Mockito.when(contactDAO.isContactExists("thierry")).thenReturn(false);
        contactService.removeContact("thierry");
    }

    @Test
    public void shouldOkIfWantRemoveContactExist() throws ContactException {
        Mockito.when(contactDAO.isContactExists("thierry")).thenReturn(true);
        contactService.removeContact("thierry");
    }

}
