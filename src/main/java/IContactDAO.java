import java.util.ArrayList;

public interface IContactDAO {

    boolean isContactExists(String nom);

    void add(Contact c);

    void remove(Contact c);
}
