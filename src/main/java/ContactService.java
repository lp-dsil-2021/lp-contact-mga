
import java.util.ArrayList;
import java.util.Locale;

/**
	* ContactService
	*/
public class ContactService {
    private IContactDAO contactDAO;


    public ContactService(){
        this.contactDAO = new ContactDAO();
    }

    public void creerContact(String nom) throws ContactException {
        if(nom == null){
            throw new ContactException();
        }
        if(nom.trim().length() < 3 || nom.trim().length() > 41){
            throw new ContactException();
        }
        if(!nom.trim().matches("[a-zA-Z]+\\.?")){
            throw new ContactException();
        }
        if(this.contactDAO.isContactExists(nom)){
            throw new ContactException();
        }
        Contact c = new Contact(nom);
        this.contactDAO.add(c);
    }

    public void removeContact(String nom) throws ContactException{
        if(!this.contactDAO.isContactExists(nom)){
            throw new ContactException();
        }
    }


}