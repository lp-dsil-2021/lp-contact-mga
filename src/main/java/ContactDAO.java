import java.util.ArrayList;
import java.util.Locale;

public class ContactDAO implements IContactDAO {
    private ArrayList<Contact> list;

    public ContactDAO(){
        this.list = new ArrayList<Contact>();
    }

    public void add(Contact c){
        this.list.add(c);
    }

    public boolean isContactExists(String nom){
        return list.contains(new Contact(nom));
    }

    public void remove(Contact c){this.list.remove(c);}
}
